/***************************************************************************
 *   Copyright (C) 2007 by Alexandru Scvortov   *
 *   scvalex@gmail.com   *
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 *   This program is distributed in the hope that it will be useful,       *
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of        *
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the         *
 *   GNU General Public License for more details.                          *
 *                                                                         *
 *   You should have received a copy of the GNU General Public License     *
 *   along with this program; if not, write to the                         *
 *   Free Software Foundation, Inc.,                                       *
 *   59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.             *
 ***************************************************************************/


#ifndef DOCVIEWER_H
#define DOCVIEWER_H

#include <QWidget>

class Environment;
class QString;
class QAssistantClient;
class QShowEvent;

/**
	@author Alexandru Scvortov <scvalex@gmail.com>
*/
class DocViewer : public QWidget {
    Q_OBJECT

public:
    DocViewer(QWidget *_parent = 0);

    ~DocViewer();

    void show();

private:
    Environment *env;

    QString docDir;
};

#endif
